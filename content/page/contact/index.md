---
title: Contacto
layout: "contact"
slug: "contacto"
menu:
    main:
        name: Contacto
        weight: -70
        params: 
            icon: message
---

Si tiene alguna duda, sugerencia o cualquier otro tipo de asunto acerca del contenido de este blog de las tecnologías de la información, le invito a que me envíe un mensaje a la dirección de correo electrónico:

**contacto.beti@protonmail.com**

Agradezco profundamente su interés hacia los artículos de mi blog y espero que estos le hayan sido de utilidad. 
