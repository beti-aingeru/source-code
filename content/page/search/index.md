---
title: Búsqueda
layout: "search"
slug: "buscador"
outputs:
    - html
    - json
menu:
    main:
        name: Búsqueda
        weight: -90
        params: 
            icon: search
---