class RecommendationsWidget {
    private recommendationsList: HTMLElement;
    private recommendations: NodeListOf<HTMLElement>;

    constructor() {
        this.recommendationsList = document.querySelector('.recommendations--list') as HTMLUListElement;
        this.recommendations = this.recommendationsList?.querySelectorAll('.recommendation');
        this.recommendations?.forEach(currentRecommendation => {
            this.createRecommendationView(currentRecommendation as HTMLElement, 'add');
        });
    }

    static checkExistenceRecommendationsSection(): boolean {
        const recommendationsSection = document.querySelector('.recommendations') as HTMLUListElement;
        return !!recommendationsSection;
    }

    private createRecommendationButton(recommendation: HTMLElement, buttonSvgIcon: SVGElement, buttonText: string, event: () => void): void {
        const recommendationButton: HTMLElement = document.createElement('button');
        recommendationButton.className = 'recommendation-button';
        recommendationButton.appendChild(buttonSvgIcon);
        recommendationButton.insertAdjacentText('beforeend', buttonText);        
        recommendationButton.addEventListener('click', event);
        recommendation.appendChild(recommendationButton);
    }

    private createButtonSvg(svgContent: string): SVGElement {
        const buttonSvg = document.createElement('svg');
        buttonSvg.innerHTML = svgContent;
        return buttonSvg;
    }
      
    private createExpandButton(recommendation: HTMLElement): void {
        const expandIcon = this.createButtonSvg('<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-window-maximize" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M3 16m0 1a1 1 0 0 1 1 -1h3a1 1 0 0 1 1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1z" /><path d="M4 12v-6a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-6" /><path d="M12 8h4v4" /><path d="M16 8l-5 5" /></svg>');
        this.createRecommendationButton(recommendation, expandIcon, 'Mostrar más información', () => {
            this.expandRecommendation(recommendation);
        });
    }
    
    private createMinimizeButton(recommendation: HTMLElement): void {
        const minimizeIcon =  this.createButtonSvg('<svg xmlns="http://www.w3.org/2000/svg" class="icon icon-tabler icon-tabler-window-minimize" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><path d="M3 16m0 1a1 1 0 0 1 1 -1h3a1 1 0 0 1 1 1v3a1 1 0 0 1 -1 1h-3a1 1 0 0 1 -1 -1z" /><path d="M4 12v-6a2 2 0 0 1 2 -2h12a2 2 0 0 1 2 2v12a2 2 0 0 1 -2 2h-6" /><path d="M15 13h-4v-4" /><path d="M11 13l5 -5" /></svg>');
        this.createRecommendationButton(recommendation, minimizeIcon, 'Minimizar', () => {
            this.minimizeRecommendation(recommendation);
        });
    }
    
    private toggleParagraphsHiddenClass(paragraphs: HTMLCollection, action: string): void {
        if (action === 'add' || action === 'remove') {
            for (let paragraphNumber: number = 1; paragraphNumber < paragraphs.length; paragraphNumber++) {
                const paragraph: HTMLElement = paragraphs[paragraphNumber] as HTMLElement;
                paragraph.classList[action]('recommendation-hidden-paragraph');
            }
        }
    }
    
    private createRecommendationView(recommendation: HTMLElement, action: string): void {
        let paragraphs: HTMLCollection = recommendation.getElementsByTagName('p');
        if (paragraphs.length > 1) {
            this.toggleParagraphsHiddenClass(paragraphs, action);
            recommendation.querySelector("button")?.remove();
            action === 'add' ? this.createExpandButton(recommendation) : this.createMinimizeButton(recommendation);
        }
    }
    
    private expandRecommendation(recommendation: HTMLElement): void {
        this.recommendations.forEach(loopRecommendation => {
            if (loopRecommendation != recommendation) {
                this.recommendationsList.removeChild(loopRecommendation);
            }
        });
        this.createRecommendationView(recommendation, 'remove');
    }
    
    private minimizeRecommendation(recommendation: HTMLElement): void {
        this.recommendationsList.removeChild(recommendation);
        this.recommendations.forEach(loopRecommendation => {
            this.recommendationsList.appendChild(loopRecommendation);
        });
        this.createRecommendationView(recommendation, 'add');
    }
}
  
export default RecommendationsWidget;